module.exports = function(sequelize, DataTypes) {
	var Sales = sequelize.define('table_sales', {
		table_id: DataTypes.INTEGER,
    total_sale: DataTypes.DOUBLE
	});
  return Sales;
};