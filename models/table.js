module.exports = function(sequelize, DataTypes) {
	var Table = sequelize.define('tables', {
		state: DataTypes.BOOLEAN,
    position_x: DataTypes.DOUBLE,
    position_y: DataTypes.DOUBLE
	});
 
  Table.dishes = function (id) {
    return sequelize.query(
      "SELECT t.id AS table_id,d.dish_name, d.price, d.id,COUNT(d.id) AS cant,d.price *COUNT(d.id) AS total " +
      "FROM tables t " +
      "INNER JOIN table_dishes td ON (td.table_id = t.id) " +
      "INNER JOIN dishes d ON (td.dish_id = d.id) " +
      "WHERE t.id = " + id +
      " GROUP BY t.id, d.dish_name,d.price, d.id", {type: sequelize.QueryTypes.SELECT})
  }
  Table.waiters = function (id) {
    return sequelize.query(
      "SELECT u.name AS waiter, t.id " +
      "FROM users u " +
      "INNER JOIN user_tables ut ON (u.id = ut.user_id) " +
      "INNER JOIN tables t ON (ut.table_id = t.id) " +
      "WHERE t.id = " + id, {type: sequelize.QueryTypes.SELECT});
  }

  Table.allSales = function () {
    return sequelize.query(
      "SELECT t.id AS table_id, SUM(ts.total_sale) AS earning " + 
      "FROM tables t " + 
      "LEFT JOIN table_sales ts ON (ts.table_id = t.id) " +
      " GROUP BY t.id",{type: sequelize.QueryTypes.SELECT});
  }

  return Table;
};  