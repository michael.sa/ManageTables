module.exports = function(sequelize, DataTypes) {
	var Dishes = sequelize.define('table_dishes', {
        dish_id: DataTypes.INTEGER,
				table_id: DataTypes.INTEGER
	});
 
  return Dishes;
}; 