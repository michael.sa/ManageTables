module.exports = function(sequelize, DataTypes) {
	var User = sequelize.define('users', {
		name: DataTypes.STRING,
		user_name: DataTypes.STRING,
		password: DataTypes.STRING
	});
  User.allEarnings = function () {
    return sequelize.query(
      "SELECT u.name AS waiter,SUM(ts.total_sale) AS gain, SUM(ts.total_sale)/10 AS commission " +
      "FROM users u " +
      "LEFT JOIN user_tables ut ON (ut.user_id = u.id) " +
      "LEFT JOIN tables t ON (t.id = ut.table_id) " +
      "LEFT JOIN table_sales ts ON (ts.table_id = t.id) " +
      " GROUP BY u.id",{type: sequelize.QueryTypes.SELECT})
  }

  return User;
};