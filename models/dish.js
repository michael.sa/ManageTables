module.exports = function(sequelize, DataTypes) {
	var Dish = sequelize.define('dishes', {
		dish_name: DataTypes.STRING,
        price: DataTypes.DOUBLE
	});

 return Dish;
};  