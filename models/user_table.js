module.exports = function(sequelize, DataTypes) {
	var Waiters = sequelize.define('user_tables', {
		table_id: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER
	});
  return Waiters;
};