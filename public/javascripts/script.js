
// target elements with the "draggable" class
$(document).ready(function () {
  popup();
  interact('.draggable')
    .draggable({
      // enable inertial throwing
      inertia: true,
      // keep the element within the area of it's parent
      restrict: {
        restriction: "parent",
        endOnly: true,
        elementRect: {
          top: 0,
          left: 0,
          bottom: 1,
          right: 1
        }
      },
      // enable autoScroll
      autoScroll: true,

      // call this function on every dragmove event
      onmove: dragMoveListener,
      onend: dragEnd
    });
  var coordx, coordy;
  function dragMoveListener(event) {
    var target = event.target,
      // keep the dragged position in the data-x/data-y attributes
      x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
      y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

    // translate the element
    target.style.webkitTransform =
      target.style.transform =
      'translate(' + x + 'px, ' + y + 'px)';

    // update the posiion attributes
    target.setAttribute('data-x', x);
    target.setAttribute('data-y', y);

    coordx = x;
    coordy = y;
    $('#cordenadas ').text(Math.round(x) + '\n\n' + Math.round(y) + "id" + target.id);
  }
  function dragEnd(event) {
    var target = event.target;

    updatePosition(target.id, coordx, coordy);
  }
  // this is used later in the resizing and gesture demos
  window.dragMoveListener = dragMoveListener;

  $('.btnFondo').click(function () {
    $('#marco').css("background-image", "url(http://www.inviertetuvida.com/wp-content/uploads/2016/03/fondo-azul-jpg.jpg)");
  });
});

//pop Up de platillos
function popup() {
  $('.open').dblclick(function () {
    $('.popup').fadeIn('slow');
    $('.popup-overlay').fadeIn('slow');
    $('.popup-overlay').height($(window).height());

  });

  $('.close').click(function () {
    $('.popup').fadeOut('slow');
    $('.popup-overlay').fadeOut('slow');
    return false;
  });
}

