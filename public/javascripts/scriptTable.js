var BackGroungs = {
      getData: function () {
            return $.getJSON('/api/v1/backgrounds');
      }
};

var changeBackground = function () {
      BackGroungs.getData().done(function (json) {

            json.backgrounds.forEach(function (background) {
                  console.log('back' + background.background);
                  $('#id').val(background.id);
                  $('#marco').attr('style', 'background-image:url("' + background.background + '")');
            })
      })
}
$(document).ready(function () {
      changeBackground();
      $('.dropdown-menu.back-ground').on('click', '.change', function (event) {
            event.preventDefault();
            $('#panel').show();
            $('#fileToUpload').change(function () {
                  $('#img').attr("src", window.URL.createObjectURL(this.files[0]));
            })
      });
      $('#cancel').on('click', function () {
            $('#panel').hide();
      })

      //Ocultamos el menú al cargar la página
      $("#context").hide();

      //cuando hagamos click, el menú desaparecerá
      $(document).click(function (e) {
            if (e.button == 0) {
                  $("#context").css("display", "none");
            }
      });

      //si pulsamos escape, el menú desaparecerá
      $(document).keydown(function (e) {
            if (e.keyCode == 27) {
                  $("#context").css("display", "none");
            }
      });
      // //controlamos los botones del menú
      $("#context").click(function (e) {

            // El switch utiliza los IDs de los <li> del menú
            switch (e.target.id) {
                  case "showSales":

                        $('.popupFactura').fadeIn('slow');
                        $('.popup-overlay').fadeIn('slow');
                        $('.popup-overlay').height($(window).height());
                        hideMenu();
                        getWaiters();
                        showWaiter();

                        break;
                  case "deleteTable":
                        deleteTable();
                        break;
                  case "trueTable":
                        hideMenu();
                        statuTrue(idDivSelect);
                        break;
                  case "assingSale":
                        $('.popup1').fadeIn('slow');
                        $('.popup-overlay').fadeIn('slow');
                        $('.popup-overlay').height($(window).height());
                        hideMenu();
                        //llamar a cargar los datos
                        getWaiters();
                        break;
            }

      });

      $('.close').click(function () {
            $('.popup1').fadeOut('slow');
            $('.popup-overlay').fadeOut('slow');
            return false;
      });

      $('.close').click(function () {
            $('.popupFactura').fadeOut('slow');
            $('.popup-overlay').fadeOut('slow');
            return false;
      });
      function closePopPupPay() {
            $('.close').click(function () {
                  $('.popupPay').fadeOut('slow');
                  $('.popup-overlay').fadeOut('slow');
                  return false;
            });

      }
      function hideMenu() {
            $("#context").css("display", "none");
      }

});
