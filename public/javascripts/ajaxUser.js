var Users = {
    getData: function () {
        return $.getJSON('/api/v1/users');
    },
    insert: function (data) {
        return $.post('/api/v1/users', data);
    },
    delete: function (id) {
        return $.ajax({ url: '/api/v1/users/' + id, type: 'DELETE' })
    },
    update: function (id, data) {
        return $.ajax({
            url: '/api/v1/users/' + id,
            type: 'PUT',
            dataType: 'json',
            data: data,
        });
    },
    assignWaiter: function (data) {
        return $.post('/api/v1/users/assign', data);
    },
    getEarnings: function () {
        return $.getJSON('/api/v1/users/earnings');
    }
};

function getUsers() {
    Users.getData().done(function (json) {
        $('#waiters tr:not(:first)').remove();
        json.users.forEach(function (user) {
            $('#new input').val('');
            var tabla = $('#waiters');
            tabla.append(
                '<tr>' +
                '<td class="name" value=' + user.name + '>' + user.name + '</td>' +
                '<td class="user_name" value=' + user.user_name + '>' + user.user_name + '</td>' +
                '<td>' +
                '<button class="btn btn-info btn-circle edit" id=' + user.id + '><i class="glyphicon glyphicon-pencil"></button>' +
                '</td>' +
                '<td>' +
                '<button type="button" class="btn btn-danger btn-circle delete" id=' + user.id + '><i class="glyphicon glyphicon-remove"></button>' +
                '</td>' +
                '</tr>');
        });
    });
}
function getWaiters() {
    //verifica para ver si, se muestra el select
    $('#loadSales').empty();
    Tables.getData().done(function (json) {
        if (json.tables.length != 0) {
            json.tables.forEach(function (table) {
                if (table.id == idDivSelect) {
                    debugger
                    if (table.state == true) {
                        loadWaiter();
                        //oculto la factura
                        $('.popupFactura').fadeOut('slow');
                        $('.popup-overlay').fadeOut('slow');

                    } else {
                        $('.popup1').fadeOut('slow');
                        $('.popup-overlay').fadeOut('slow');
                    }
                }
            });
        }
    });
}

function getEarnings() {
    var earned;
    var commission;
    Users.getEarnings().done(function (json) {
        $('#allEarnings tr:not(:first)').remove();
        json.usersEarnings.forEach(function (userEarning) {
            if (userEarning.gain != null && userEarning.commission != null) {
                earned = userEarning.gain;
                commission = userEarning.commission;
            } else {
                earned = 0;
                commission = 0;
            }
            var tabla = $('#allEarnings');
            tabla.append(
                '<tr>' +
                '<td class="name" value=' + userEarning.waiter + '>' + userEarning.waiter + '</td>' +
                '<td class="user_name" value=' + userEarning.waiter + '>' + earned + '</td>' +
                '<td value=' + userEarning.commission + '>' + commission + '</td>' +
                '</tr>');
        });
    });
}
//load select to waitress if available
function loadWaiter() {
    Users.getData().done(function (json) {
        var elem = document.createElement("select");
        elem.setAttribute("id", 'select' + idDivSelect);
        elem.setAttribute("class", "form-control selectWaitres");
        json.users.forEach(function (user) {
            var opt = document.createElement('option');
            opt.value = user.id;
            opt.innerHTML = user.name;
            elem.appendChild(opt);
        });
        $('#loadSales').append(elem);
        evento();
    });
}

function assignWaiter(id_waiter) {

    var dato = Users.assignWaiter({ table_id: idDivSelect, user_id: id_waiter }).done(
    );
    statuFalse(idDivSelect);

}
function evento() {
    $('.selectWaitres').click(function (e) {
        id = $('#select' + idDivSelect).val();
        assignWaiter(id);
    });
}

$(document).ready(function () {
    getEarnings();
    getUsers();
    var id_table;
    $('#add').on('click', function () {
        $('#edit').show();
        $('#userPassword').show();
        $('#new input').val('');
    });

    $('#cancel').on('click', function () {
        $('#edit').hide();
    });

    $('#waiters').on('click', '.edit', function (event) {
        $('#edit').show();
        $('#userPassword').hide();
        $('#name').val($(this).closest("tr").find(".name").text());
        $('#user_name').val($(this).closest("tr").find(".user_name").text());
        $('#id').val($(this).attr('id'));
    });

    $('#save').click(function (event) {
        var id = $('#id').val();
        var name = $('#name').val();
        var user_name = $('#user_name').val();
        var password = $('#password').val();
        if (id) {
            Users.update(id, { name: name, user_name: user_name }).done(getUsers());
            return;
        } else if (password) {
            Users.insert({ name: name, user_name: user_name, password: password }).done(getUsers());
            return;
        }
        alert('Please Complete all The Fields');
    });

    $('#waiters').on('click', '.delete', function (event) {
        Users.delete($(this).attr('id')).done(getUsers);
        $('#edit').hide();
    });
});