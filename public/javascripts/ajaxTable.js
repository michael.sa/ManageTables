var idDivSelect; // one click
var id_table; // two click
var Tables = {
    getData: function () {
        return $.getJSON('/api/v1/tables');
    },
    insert: function (data) {
        return $.post('/api/v1/tables', data);
    },
    savePay: function (id, data) {
        return $.post('/api/v1/tables/' + id, data);
    },
    update: function (id, data) {
        return $.ajax({
            url: '/api/v1/tables/' + id,
            type: 'PUT',
            dataType: 'json',
            data: data,
        });
    },
    delete: function (id) {
        return $.ajax({ url: '/api/v1/tables/' + id, type: 'DELETE' });
    },
    addDish: function (data) {
        return $.post('/api/v1/tables/add', data);
    },
    getTableInf: function (id_table) {
        return $.getJSON('/api/v1/tables/inf/' + id_table);
    },
    getEarning: function () {
        return $.getJSON('/api/v1/tables/sales');
    }
};

var refresh = function () {
    Tables.getData().done(function (json) {
        if (json.tables.length != 0) {
            $('#marco').empty();
            json.tables.forEach(function (table) {
                var elem = document.createElement("div");
                elem.setAttribute("id", table.id);
                elem.setAttribute("class", "open draggable divTable");
                elem.setAttribute("data-x", table.position_x);
                elem.setAttribute("data-y", table.position_y);
                elem.setAttribute("style", "transform:translate(" + table.position_x + "px" + "," + table.position_y + "px)");

                if (table.state == true) {
                    elem.style.backgroundImage = "url(/images/tableTrue.png)";
                    $('#select' + idDivSelect).prop('disabled', false);
                } else {
                    elem.style.backgroundImage = "url(/images/tableFalse.png)";
                    $('#select' + idDivSelect).prop('disabled', true);
                }
                $('#marco').append(elem);
                $('#' + table.id).append("<b style='color: lightskyblue'>" + table.id + "</b>");
                popup();
                $('.divTable').bind("contextmenu", function (e) {
                    $("#context").css({ 'display': 'block', 'left': e.pageX, 'top': e.pageY });
                    idDivSelect = event.target.id;
                    return false;
                });
            });
        } else {

        }
    });

}
// load dishes in Pop UP
function getTableDishes() {
    Tables.getTableInf(id_table).done(function (json) {
        $('#datos tr:not(:first)').remove();
        $('#waiter').empty();
        json.dishes.forEach(function (dish) {
            $('#form input').val('');
            var tabla = $('#datos');
            tabla.append(
                '<tr>' +
                '<td>' + dish.dish_name + '</td>' +
                '<td>' + dish.price + '</td>' +
                '<td>' + dish.cant + '</td>' +
                '</tr>');
        });
        json.waiters.forEach(function (waiter) {
            $('#waiter').text('Waiter: ' + waiter.waiter);
        })
    });
}

function getTableEarning() {
    var gain;
    Tables.getEarning().done(function (json) {
        $('#tableEarning tr:not(:first)').remove();
        json.earnings.forEach(function (earning) {
            if (earning.earning != null) {
                gain = earning.earning;
            } else {
                gain = 0;
            }
            var tabla = $('#tableEarning');
            tabla.append(
                '<tr>' +
                '<td>' + earning.table_id + '</td>' +
                '<td>' + gain + '</td>' +
                '</tr>');
        });
    });
}

$(document).ready(function () {
    getTableEarning();
    $('#marco').on("click", '.divTable', function (event) {
        id_table = $(this).attr('id');
        getTableDishes();

    });

    $('.dropdown-menu.create').on('click', '.create_tale', function (event) {
        event.preventDefault();
        var state = true;
        var position_x = 5;
        var position_y = 1;
        var dato = Tables.insert({ state: state, position_x: position_x, position_y: position_y }).done(
            refresh);
        return;
    });

    $("#addDish").click(function (e) {
        id = $("#selectDish").val();
        Tables.addDish({ dish_id: id, table_id: id_table }).done(
            getTableDishes
        )

    });
});

function updatePosition(id, x, y) {

    Tables.update(id, { position_x: x, position_y: y }).done(refresh);

}
function statuTrue(id) {
    var state = true;
    Tables.update(id, { state: state }).done(refresh);

}
function statuFalse(id) {
    var state = false;


    Tables.update(id, { state: state }).done(refresh);

}
function deleteTable() {
    Tables.delete(idDivSelect).done(refresh);
}
refresh();