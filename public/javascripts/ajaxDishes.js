var Dishes = {
    getData: function () {
        return $.getJSON('/api/v1/dishes');
    },
    insert: function (data) {
        return $.post('/api/v1/dishes', data);
    },
    delete: function (id) {
        return $.ajax({ url: '/api/v1/dishes/' + id, type: 'DELETE' })
    },
    update: function (id, data) {
        return $.ajax({
            url: '/api/v1/dishes/' + id,
            type: 'PUT',
            dataType: 'json',
            data: data,
        });
    }
};

function getDishes() {
    Dishes.getData().done(function (json) {
        $('#dishes tr:not(:first)').remove();
        $('#selectDish').empty();
        json.dishes.forEach(function (dish) {
            $('#form input').val('');
            var tabla = $('#dishes');
            tabla.append(
                '<tr>' +
                '<td class="name" value=' + dish.dish_name + '>' + dish.dish_name + '</td>' +
                '<td class="price" value=' + dish.price + '>' + dish.price + '</td>' +
                '<td>' +
                '<button class="btn btn-info btn-circle edit" id=' + dish.id + '><i class="glyphicon glyphicon-pencil"></button>' +
                '</td>' +
                '<td>' +
                '<button type="button" class="btn btn-danger btn-circle delete" id=' + dish.id + '><i class="glyphicon glyphicon-remove"></button>' +
                '</td>' +
                '</tr>');

            $('#selectDish').append('<option value=' + dish.id + '>' + dish.dish_name + '</option>');
        });
    });
}

$(document).ready(function () {
    getDishes();

    $('#add').on('click', function () {
        $('#edit').show();
        $('#dish_name').val('');
        $('#price').val('');
        $('#id').val('');
    });

    $('#cancel').on('click', function () {
        $('#edit').hide();
    });
    //edit dishes and load data
    $('#dishes').on('click', '.edit', function (event) {
        $('#edit').show();
        $('#dish_name').val($(this).closest("tr").find(".name").text());
        $('#price').val($(this).closest("tr").find(".price").text());
        $('#id').val($(this).attr('id'));
    });

    $('#save').click(function (event) {
        var id = $('#id').val();
        var dish_name = $('#dish_name').val();
        var price = $('#price').val();
        if (id) {
            Dishes.update(id, { dish_name: dish_name, price: price }).done(getDishes());
            return;
        }
        Dishes.insert({ dish_name: dish_name, price: price }).done(getDishes());
    });

    $('#dishes').on('click', '.delete', function (event) {
        Dishes.delete($(this).attr('id')).done(getDishes);
        $('#edit').hide();
    });
    function agregarEvento() {
        $('#idBtnDish').click(function (e) {
            alert(e.target.id);
        });
    }

    $('.btnDish').click(function (e) {
        alert(e.target.id);
    });

});

