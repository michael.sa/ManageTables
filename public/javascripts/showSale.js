var totalPay = 0;
//show dishes add to client
function showWaiter() {
    Tables.getTableInf(idDivSelect).done(function (json) {
        totalPay = 0;
        $('#waiterName').empty();
        if (json != null) {
            json.waiters.forEach(function (waiter) {
                $('#waiterName').text('Waiters :' + waiter.waiter.toUpperCase());
            });
        }
        if (json.dishes != null) {
            $('#LoadDishes tr:not(:first)').remove();
            $('#form input').val('');

            json.dishes.forEach(function (dishes) {
                var tabla = $('#LoadDishes');
                var number = parseInt(dishes.total);
                totalPay = totalPay + number;
                tabla.append(
                    '<tr>' +
                    '<td>' + dishes.dish_name + '</td>' +
                    '<td>' + dishes.price + '</td>' +
                    '<td>' + dishes.cant + '</td>' +
                    '<td>' + dishes.total + '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td>Total : ' + totalPay + '</td>' +
                    '</tr>'
                );
                //create boton Pay
                $('#divActionPay').empty();
                var button = '<input type="button" class="btn btn-success" value="Pay" id="btnPay" name="Pay"/>';
                $('#divActionPay').append(button);
                var buttonDivi = '<input type="button" class="btn btn-primary" value="Paid divided " id="buttonDivi" name="Pay"/>';
                $('#divActionPay').append(buttonDivi);
                eventCancel();
                eventDiv();
            });

        }
    });

}
//    button click
function eventCancel() {
    $("#btnPay").click(function () {
        Pay();
    });
}

/**
 * cambiar el nombre a eventDiv
 */
function eventDiv() {
    $("#buttonDivi").click(function () {
        $('.popupPay').fadeIn('slow');
        $('.popup-overlay').fadeIn('slow');
        $('.popup-overlay').height($(window).height());
    });
}

function Pay() {
    Tables.savePay(idDivSelect, { table_id: idDivSelect, total_sale: totalPay }).done(
        alert('Account canceled')
    );

    statusOriginal();
    //clear input
    $('#waitresName').empty();
    $('#LoadDishes').empty();
    $('#inputNumberDiv').empty();
    $('#unitPay').empty();
    $('.popupPay').fadeOut('slow');
    $('.popup-overlay').fadeOut('slow');
    location.reload();
}

$(document).ready(function () {
    $('#inputNumberDiv').change(function () {
        var u = $('#inputNumberDiv').val();
        var op = totalPay / u;
        $("#unitPay").text('c/u :' + Math.round(op));
    });

    $('#btnPayDivi').click(function () {
        Pay();
    })
});
function statusOriginal() {
    statuTrue(idDivSelect);
    $('.popupFactura').fadeOut('slow');
    $('.popup-overlay').fadeOut('slow');
    $('#loadFactura').empty();
}