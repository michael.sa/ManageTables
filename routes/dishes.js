var express = require('express');
var router = express.Router();
var models = require('../models');


var criteria = function (req) {
  return { where: { id: req.params.id } };
}

/* GET dishes listing. */
router.get('/', function (req, res, next) {
  if (req.session.logged) {
    res.format({
      json: function () {
        models.dishes.findAll().then(dishes => {
          res.json({ dishes: dishes });
        });
      },
      html: function () {
        models.dishes.all().then(dishes => {
          res.render('dishes/index', { dishes: dishes });
        });
      }
    });
    return;
  }
  res.render('users/login', { message: '*Please Login' });
});

router.get('/new', function (req, res, next) {
  if (req.session.logged) {
    res.render('dishes/new');
    return;
  }
  res.render('users/login', { message: '*Please Login' });
});

router.get('/:id', function (req, res, next) {
  if (req.session.logged) {
    var dish = models.dishes.findOne(criteria(req));
    res.format({
      json: function () {
        dish.then(dish => {
          res.json(dish);
        });
      },
      html: function () {
        dish.then(dish => {
          res.render('dishes/edit', { dish: dish });
        });
      }
    });
    return;
  }
  res.render('users/login', { message: '*Please Login' });
});

router.post('/', function (req, res, next) {
  if (req.session.logged) {
    var dish = models.dishes.create(req.body);
    res.format({
      json: function () {
        dish.then(dish => {
          res.json(dish);
        });
      },
      html: function () {
        dish.then(dish => {
          res.redirect('/api/v1/dishes');
        });
      }
    })
    return;
  }
  res.render('users/login', { message: '*Please Login' });
});

router.delete('/:id', function (req, res, next) {
  if (req.session.logged) {
    res.format({
      json: function () {
        models.dishes.destroy(criteria(req)).then(() => {
          res.json({ status: 'ok' });
        });
      },
      html: function () {
        models.dishes.destroy(criteria(req)).then(() => {
          res.redirect('/api/v1/dishes');
        });
      }
    });
    return;
  }
  res.render('users/login', { message: '*Please Login' });
});

router.put('/:id', function (req, res, next) {
  if (req.session.logged) {
    res.format({
      json: function () {
        models.dishes.update(req.body, criteria(req)).then(dish => {
          res.json(dish);
        });
      },
      html: function () {
        models.dishes.update(req.body, criteria(req)).then(dish => {
          res.redirect('/api/v1/dishes');
        });
      }
    });
    return;
  }
  res.render('users/login', { message: '*Please Login' });
});


module.exports = router;
