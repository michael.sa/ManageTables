var express = require('express');
var router = express.Router();
var models = require('../models');

var criteria = function (req) {
  return { where: { id: req.params.id } };
}

/* GET users listing. */
router.get('/', function (req, res, next) {
  if (req.session.logged) {
    res.format({
      json: function () {
        models.users.findAll().then(users => {
          res.json({ users: users });
        });
      },
      html: function () {
        models.users.all().then(users => {
          res.render('users/index', { users: users });
        });
      }
    });
    return
  }
  res.render('users/login', { message: '*Please Login' });
});

router.get('/signin', function (req, res, next) {
    res.render('users/signin');
});

/**
 * get all users earnings 
 */
router.get('/earnings', function (req, res, next) {
  if (req.session.logged) {
    res.format({
      json: function () {
        models.users.allEarnings().then(usersEarnings => {
          res.json({ usersEarnings: usersEarnings });
        });
      },
      html: function () {
        models.users.all().then(usersEarnings => {
          res.render('users/sales', { usersEarnings: usersEarnings });
        });
      }
    });
    return;
  }
  res.render('users/login', { message: '*Please Login' });
});


router.get('/login', function (req, res, next) {
  res.render('users/login');
});

router.get('/logout', function (req, res, next) {
  if (req.session.logged) {
    req.session.destroy();
    res.format({
      json: function () {
        res.status(200).json('logged out');
      },
      html: function () {
        res.redirect('/api/v1/users/login');
      }
    });
  }
});

router.get('/:id', function (req, res, next) {
  if (req.session.logged) {
    var user = models.users.findOne(criteria(req));
    res.format({
      json: function () {
        user.then(user => {
          res.json(user);
        });
      },
      html: function () {
        user.then(user => {
          res.render('users/edit', { user: user });
        });
      }
    })
    return;
  }
  res.render('users/login', { message: '*Please Login' });
});

router.get('/sales/:id', function (req, res, next) {
  if (req.session.logged) {
    var user_sales = models.users.sales(req.params.id);
    res.format({
      json: function () {
        user_sales.then(user_sales => {
          res.json(user_sales);
        });
      },
      html: function () {
        user_sales.then(user_sales => {
          res.render('users/edit', { user_sales: user_sales });
        });
      }
    })
    return;
  }
  res.render('users/login', { message: '*Please Login' });
});

router.post('/login', function (req, res, next) {
  var user = models.users.findOne({ where: { user_name: req.body.user_name, password: req.body.password } });
  user.then(user => {
    if (user) {
      res.format({
        json: function () {
          req.session.logged = true;
          req.session.user_name = user.user_name;
          res.json(user);
        },
        html: function () {
          req.session.logged = true;
          req.session.user_name = user.user_name;
          res.redirect('/api/v1/tables');
        }
      });
      return;
    }
    res.render('users/login', { message: '*invalid User Name or Password' });
  });
});

/**
 * assign waiter
 */
router.post('/assign', function (req, res, next) {
  if (req.session.logged) {
    var user_tables = models.user_tables.create(req.body);
    res.format({
      json: function () {
        user_tables.then(user_tables => {
          res.json(user_tables);
        });
      },
      html: function () {
        user_tables.then(user_tables => {
          res.redirect('/api/v1/tables');
        });
      }
    });
    return;
  }
  res.json('Not logged');
});


router.post('/', function (req, res, next) {
  var user = models.users.create(req.body);
  res.format({
    json: function () {
      user.then(user => {
        res.json(user);
      });
    },
    html: function () {
      user.then(user => {
        res.redirect('/api/v1/users');
      });
    }
  })
});

router.delete('/:id', function (req, res, next) {
  if (req.session.logged) {
    res.format({
      json: function () {
        models.users.destroy(criteria(req)).then(() => {
          res.json({ status: 'ok' });
        });
      },
      html: function () {
        models.users.destroy(criteria(req)).then(() => {
          res.redirect('/api/v1/users');
        });
      }
    });
    return;
  }
  res.render('users/login', { message: '*Please Login' });
});

router.put('/:id', function (req, res, next) {
  if (req.session.logged) {
    res.format({
      json: function () {
        models.users.update(req.body, criteria(req)).then(user => {
          res.json(user);
        });
      },
      html: function () {
        models.users.update(req.body, criteria(req)).then(user => {
          res.redirect('/api/v1/users');
        });
      }
    });
    return;
  }
  res.render('users/login', { message: '*Please Login' });
});
module.exports = router;
