var express = require('express');
var router = express.Router();
var models = require('../models');
var app = express();

var criteria = function(req) {
  return { where: {id: req.params.id} };
}

router.get('/', function(req, res, next) {
  if (req.session.logged) {
    res.format({
      json: function () {
        models.backgrounds.findAll().then(backgrounds => {
          res.json({backgrounds: backgrounds});
        });
      },
      html: function () {
        models.backgrounds.all().then(backgrounds => {
          res.render('tables/index', { backgrounds: backgrounds });
        });
      }
    });
    return;
  }
  res.render('users/login', { message: '*Please Login' });
});

router.post('/', function(req, res, next) {
    res.json(req.body.id);
  if(req.files){
    var picture = req.files.background;
    req.body.background = '/images/' + picture.name;
  }
  if (req.body.id) {
    var background = models.backgrounds.destroy({where: {id:req.body.id}});
  }  
  var background = models.backgrounds.create(req.body);
    res.format({
      json: function () {
        background.then(background => {
          res.json(background);
        });
      },
      html: function () {
        picture.mv('public/images/' + picture.name, function(err) {
            if (err)
                return res.status(500).send(err);
                background.then(background => {
                res.redirect('/api/v1/tables');
                });  
            });
        }
   })
});
module.exports = router;