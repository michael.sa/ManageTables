var express = require('express');
var router = express.Router();
var models = require('../models');


var criteria = function (req) {
  return { where: { id: req.params.id } };
}

/* GET tables listing. */
router.get('/', function (req, res, next) {
  if (req.session.logged) {
    res.format({
      json: function () {
        models.tables.findAll().then(tables => {
          res.json({ tables: tables });
        });
      },
      html: function () {
        models.tables.all().then(tables => {

          res.render('tables/index', { tables: tables });
        });
      }
    });
    return;
  }
  res.render('users/login', { message: '*Please Login' });
});

router.get('/new', function (req, res, next) {
  if (req.session.logged) {
    res.render('tables/new');
    return;
  }
  res.render('users/login', { message: '*Please Login' });
});

/**
 * get the waiter and dishes from a table
 */
router.get('/inf/:id', function (req, res, next) {
  if (req.session.logged) {
    var dishes = models.tables.dishes(req.params.id);
    var waiter = models.tables.waiters(req.params.id);
    var result = Promise.all([dishes, waiter]);
    res.format({
      json: function () {
        result.then(results => {
          res.json({
            dishes: results[0],
            waiters: results[1]
          });
        });
      },
      html: function () {
        result.then(results => {
          res.render('/api/v1/tables', {
            dishes: results[0],
            waiters: results[1]
          });
        });
      }
    });
    return;
  }
  res.render('users/login', { message: '*Please Login' });
});


/**
 * add dish to table
 */
router.post('/add', function (req, res, next) {
  if (req.session.logged) {
    var dishs = models.table_dishes.create(req.body);
    res.format({
      json: function () {
        dishs.then(dishs => {
          res.json(dishs);
        });
      },
      html: function () {
        dishs.then(dishs => {
          res.redirect('/api/v1/tables');
        });
      }
    });
    return;
  }
  res.render('users/login', { message: '*Please Login' });
})

/**
 * register a sale 
 */
router.post('/:id', function (req, res, next) {
  if (req.session.logged) {
    //create sale
    var sale = models.table_sales.create(req.body);

    res.format({
      json: function () {
        //delete dishes
        models.table_dishes.destroy({ where: { table_id: req.params.id } }).then(() => {
          res.json({ status: 'ok' });
        });
      },
      html: function () {
        models.table_dishes.destroy(criteria(req)).then(() => {
          res.redirect('/api/v1/tables');
        });
      }
    });
    return;
  }
  res.render('users/login', { message: '*Please Login' });
});

/**
 * get all table sales (earnings)
 */
router.get('/sales', function (req, res, next) {
  if (req.session.logged) {
    res.format({
      json: function () {
        models.tables.allSales().then(earnings => {
          res.json({ earnings: earnings });
        });
      },
      html: function () {
        models.tables.allSales().then(earnings => {

          res.render('tables/sales', { earnings: earnings });
        });
      }
    });
    return;
  }
  res.render('users/login', { message: '*Please Login' });
});

router.get('/:id', function (req, res, next) {
  if (req.session.logged) {
    var table = models.tables.findOne(criteria(req));
    res.format({
      json: function () {
        table.then(table => {
          res.json(table);
        });
      },
      html: function () {
        table.then(table => {
          res.render('tables/edit', { table: table });
        });
      }
    });
    return;
  }
  res.render('users/login', { message: '*Please Login' });
});

router.post('/', function (req, res, next) {
  if (req.session.logged) {
    var table = models.tables.create(req.body);
    res.format({
      json: function () {
        table.then(table => {
          res.json(table);
        });
      },
      html: function () {
        table.then(table => {
          res.redirect('/api/v1/tables');
        });
      }
    })
    return;  
  }
  res.render('users/login', { message: '*Please Login' });
});

router.delete('/:id', function (req, res, next) {
  if (req.session.logged) {
    res.format({
      json: function () {
        models.tables.destroy(criteria(req)).then(() => {
          res.json({ status: 'ok' });
        });
      },
      html: function () {
        models.tables.destroy(criteria(req)).then(() => {
          res.redirect('/api/v1/tables');
        });
      }
    });
    return;
  }
  res.render('users/login', { message: '*Please Login' });
});

router.put('/:id', function (req, res, next) {
  if (req.session.logged) {
    res.format({
      json: function () {
        models.tables.update(req.body, criteria(req)).then(table => {
          res.json(table);
        });
      },
      html: function () {
        models.tables.update(req.body, criteria(req)).then(table => {
          res.redirect('/api/v1/tables');
        });
      }
    });
    return;
  }
  res.render('users/login', { message: '*Please Login' });
});

module.exports = router;
